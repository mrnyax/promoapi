/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package safeboda.promoapi.service.interfaces;

import org.springframework.http.ResponseEntity;
import safeboda.promoapi.models.requests.PromoCodeRequest;
import safeboda.promoapi.models.requests.ValidateRequest;

/**
 *
 * @author Patrick
 */
public interface PromoService {
    
    ResponseEntity generateCode(PromoCodeRequest codeRequest);
    
    ResponseEntity getAllCode();
    
    ResponseEntity deactivateCode(String code);
    
    ResponseEntity deactivateAllCodes(String eventName);

    ResponseEntity getAllActiveCodes();

    ResponseEntity validatePromoCode(ValidateRequest request);
    
}
