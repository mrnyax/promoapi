/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package safeboda.promoapi.service.implementations;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import safeboda.promoapi.entities.PromoCodes;
import safeboda.promoapi.models.requests.PromoCodeRequest;
import safeboda.promoapi.models.requests.ValidateRequest;
import safeboda.promoapi.repository.PromoCodesRepository;
import safeboda.promoapi.service.interfaces.PromoService;

/**
 *
 * @author Patrick
 */
@Service
public class PromoServiceImpl implements PromoService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PromoCodesRepository promoCodesRepository;

    @Override
    public ResponseEntity generateCode(PromoCodeRequest codeRequest) {

        Map<String, Object> map = new HashMap<>();

        PromoCodes promoCode = this.modelMapper.map(codeRequest, PromoCodes.class);

        promoCode.setEventId(1);
        promoCode.setIsActive(true);
        promoCode.setPromoCode(createRandomCode(8));
        promoCode.setCreatedOn(new Date(System.currentTimeMillis()));
        promoCode.setUpdatedOn(new Date(System.currentTimeMillis()));

        PromoCodes code = this.promoCodesRepository.save(promoCode);

        if (null != code) {
            map.put("message", "Code generated successfully.");
            map.put("code", code.getPromoCode());
        }

        map.put("message", "Code not generated.");
        return new ResponseEntity(map, HttpStatus.BAD_REQUEST);
    }

    public String createRandomCode(int codeLength) {

        char[] chars = "abcdefghijklmnopqrstuvwxyz1234567890".toCharArray();

        StringBuilder sb = new StringBuilder();

        Random random = new SecureRandom();

        for (int i = 0; i < codeLength; i++) {
            sb.append(chars[random.nextInt(chars.length)]);
        }

        return sb.toString().toUpperCase();
    }

    @Override
    public ResponseEntity getAllCode() {

        return new ResponseEntity(this.promoCodesRepository.findAll(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity deactivateCode(String code) {

        Map<String, Object> map = new HashMap<>();

        if (null == this.promoCodesRepository.findByPromoCode(code)) {

            map.put("message", "Code does not exist.");
            return new ResponseEntity(map, HttpStatus.BAD_REQUEST);
        }

        try {
            this.promoCodesRepository.setPromoCodeInactive(code);
        } catch (Exception e) {
            map.put("error", e.getLocalizedMessage());
            return new ResponseEntity(map, HttpStatus.BAD_REQUEST);
        }

        map.put("message", "Code deactivated.");
        return new ResponseEntity(map, HttpStatus.OK);
    }

    @Override
    public ResponseEntity deactivateAllCodes(String eventName) {

        Map<String, Object> map = new HashMap<>();

        List<Integer> uniqueList = new ArrayList<>(new HashSet<>(this.promoCodesRepository.findEventPromoCodes(eventName)));

        if (uniqueList.size() <= 0) {
            map.put("message", "No active codes found");
            return new ResponseEntity(map, HttpStatus.BAD_REQUEST);
        }

        try {
            this.promoCodesRepository.setPromoCodesInactive(uniqueList);
        } catch (Exception e) {
            map.put("error", e.getLocalizedMessage());
            return new ResponseEntity(map, HttpStatus.BAD_REQUEST);
        }

        map.put("message", "Code(s) deactivated.");
        return new ResponseEntity(map, HttpStatus.OK);
    }

    @Override
    public ResponseEntity getAllActiveCodes() {

        return new ResponseEntity(this.promoCodesRepository.findActivePromoCodes(), HttpStatus.OK);
    }

    public double getDistance(double startLat, double startLon, double endLat,
            double endLon, double el1, double el2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(endLat - startLat);
        double lonDistance = Math.toRadians(endLon - startLon);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(startLat)) * Math.cos(Math.toRadians(endLat))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }

    @Override
    public ResponseEntity validatePromoCode(ValidateRequest request) {

        Map<String, Object> map = new HashMap<>();

        PromoCodes pc = this.promoCodesRepository.findByPromoCode(request.getCode());

        if (null == pc) {

            map.put("message", "Invalid promo code.");
            return new ResponseEntity(map, HttpStatus.BAD_REQUEST);
        }

        double promoRadius = pc.getValidRadiusM();
        double distanceBetween = getDistance(
                request.getStart().getLat(),
                request.getStart().getLon(),
                request.getDestination().getLat(),
                request.getDestination().getLon(), 0, 0);

        if (distanceBetween > promoRadius) {

            map.put("message", "Promo code invalid for ride distance.");
            return new ResponseEntity(map, HttpStatus.BAD_REQUEST);
        }

        map.put("message", "Promo code valid for ride distance.");
        return new ResponseEntity(map, HttpStatus.OK);
    }

}
