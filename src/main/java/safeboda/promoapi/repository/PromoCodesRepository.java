/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package safeboda.promoapi.repository;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import safeboda.promoapi.entities.PromoCodes;

/**
 *
 * @author Patrick
 */
@Repository
public interface PromoCodesRepository extends JpaRepository<PromoCodes, Integer>{
    
    @Query("SELECT code FROM PromoCodes code WHERE code.promoCode = :code AND code.isActive = 1")
    PromoCodes findByPromoCode(@Param("code") String code);    
    
    @Query("SELECT code FROM PromoCodes code WHERE code.isActive = 1")
    List<PromoCodes> findActivePromoCodes();    
    
    @Transactional
    @Modifying
    @Query("UPDATE PromoCodes c SET c.isActive = 0 WHERE c.promoCode = :code")
    void setPromoCodeInactive(@Param("code") String code);
    
    @Query("SELECT code.eventId FROM PromoCodes code WHERE code.promoEvent.eventName = :eventName AND code.isActive = 1")
    ArrayList<Integer> findEventPromoCodes(@Param("eventName") String eventName);
    
    @Transactional
    @Modifying
    @Query("UPDATE PromoCodes c SET c.isActive = 0 WHERE c.eventId = :ids")
    void setPromoCodesInactive(@Param("ids") List<Integer> ids);
    
    @Query("SELECT code FROM PromoCodes code WHERE code.eventId = :ids")
    List<PromoCodes> getPromoCodes(@Param("ids") List<Integer> ids);
    
}
