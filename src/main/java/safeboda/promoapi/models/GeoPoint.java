/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package safeboda.promoapi.models;

import javax.validation.constraints.NotNull;

/**
 *
 * @author Patrick
 */
public class GeoPoint {
    
    @NotNull
    private Double lat;
    
    @NotNull
    private Double lon;

    public GeoPoint(Double lat, Double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "GeoPoint{" + "lat=" + lat + ", lon=" + lon + '}';
    }
    
}
