/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package safeboda.promoapi.models.requests;

/**
 *
 * @author Patrick
 */
public class PromoCodeRequest {
    
    private double amount;
    private boolean canExpire;
    private int validForMins;
    private int validRadiusM;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public boolean isCanExpire() {
        return canExpire;
    }

    public void setCanExpire(boolean canExpire) {
        this.canExpire = canExpire;
    }

    public int getValidForMins() {
        return validForMins;
    }

    public void setValidForMins(int validForMins) {
        this.validForMins = validForMins;
    }

    public int getValidRadiusM() {
        return validRadiusM;
    }

    public void setValidRadiusM(int validRadiusM) {
        this.validRadiusM = validRadiusM;
    }
}
