/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package safeboda.promoapi.models.requests;

import javax.validation.constraints.NotNull;
import safeboda.promoapi.models.GeoPoint;

/**
 *
 * @author Patrick
 */
public class ValidateRequest {
    
    private GeoPoint start;
    
    private GeoPoint destination;
    
    @NotNull
    private String code;

    public ValidateRequest() {
    }

    public ValidateRequest(GeoPoint start, GeoPoint destination, String code) {
        this.start = start;
        this.destination = destination;
        this.code = code;
    }

    public GeoPoint getStart() {
        return start;
    }

    public void setStart(GeoPoint start) {
        this.start = start;
    }

    public GeoPoint getDestination() {
        return destination;
    }

    public void setDestination(GeoPoint destination) {
        this.destination = destination;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ValidateRequest{" + "start=" + start + ", destination=" + destination + ", code=" + code + '}';
    }
    
}
