/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package safeboda.promoapi.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Patrick
 */
@Entity
@Table(name = "promo_codes")
public class PromoCodes implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PROMO_CODE_ID")
    @JsonIgnore
    private Long promoCodeId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "PROMO_CODE")
    private String promoCode;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "AMOUNT")
    private double amount;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "CAN_EXPIRE")
    private boolean canExpire;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    
    @Column(name = "VALID_FOR_MINS")
    private Integer validForMins;
    
    @Column(name = "VALID_RADIUS_M")
    private Integer validRadiusM;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "IS_ACTIVE")
    private boolean isActive;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "UPDATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    
    @Column(name = "EVENT_ID")
    @JsonIgnore
    private Integer eventId;
    
    @JoinColumn(name = "EVENT_ID", referencedColumnName = "EVENT_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private PromoEvent promoEvent;

    public PromoCodes() {
    }

    public PromoCodes(Long promoCodeId) {
        this.promoCodeId = promoCodeId;
    }

    public PromoCodes(Long promoCodeId, String promoCode, double amount, boolean canExpire, Date createdOn, boolean isActive, Date updatedOn) {
        this.promoCodeId = promoCodeId;
        this.promoCode = promoCode;
        this.amount = amount;
        this.canExpire = canExpire;
        this.createdOn = createdOn;
        this.isActive = isActive;
        this.updatedOn = updatedOn;
    }

    public Long getPromoCodeId() {
        return promoCodeId;
    }

    public void setPromoCodeId(Long promoCodeId) {
        this.promoCodeId = promoCodeId;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public boolean getCanExpire() {
        return canExpire;
    }

    public void setCanExpire(boolean canExpire) {
        this.canExpire = canExpire;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getValidForMins() {
        return validForMins;
    }

    public void setValidForMins(Integer validForMins) {
        this.validForMins = validForMins;
    }

    public Integer getValidRadiusM() {
        return validRadiusM;
    }

    public void setValidRadiusM(Integer validRadiusM) {
        this.validRadiusM = validRadiusM;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public PromoEvent getPromoEvent() {
        return promoEvent;
    }

    public void setPromoEvent(PromoEvent promoEvent) {
        this.promoEvent = promoEvent;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (promoCodeId != null ? promoCodeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PromoCodes)) {
            return false;
        }
        PromoCodes other = (PromoCodes) object;
        if ((this.promoCodeId == null && other.promoCodeId != null) || (this.promoCodeId != null && !this.promoCodeId.equals(other.promoCodeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "safeboda.promoapi.promoapi.entities.PromoCodes[ promoCodeId=" + promoCodeId + " ]";
    }
    
}
