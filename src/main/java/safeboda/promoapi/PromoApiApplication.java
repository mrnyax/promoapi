package safeboda.promoapi;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PromoApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(PromoApiApplication.class, args);
    }
    
    /**
     * Register ModelMapper bean to be used in the application
     *
     * @return ModelMapper
     */
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper;
    }
}
