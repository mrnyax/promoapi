/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package safeboda.promoapi.controllers;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import safeboda.promoapi.models.requests.PromoCodeRequest;
import safeboda.promoapi.models.requests.ValidateRequest;
import safeboda.promoapi.service.interfaces.PromoService;

/**
 *
 * @author Patrick
 */
@RestController
@RequestMapping(value = "api/promocode")
public class PromoCodesController {
    
    @Autowired
    private PromoService promoService;
    
    //Post request to generate promo code
    @PostMapping(value = "generate")
    ResponseEntity generate(@RequestBody PromoCodeRequest request){
        
        return this.promoService.generateCode(request);        
    }
    
    //Deactivate promo code
    @PostMapping(value = "deactivate")
    ResponseEntity deactivated(@RequestParam("code") String code){
        
        return this.promoService.deactivateCode(code);
    }
    
    //Deactivate all promo codes
    @PostMapping(value = "deactivate/all")
    ResponseEntity deactivatedAll(@RequestParam("eventName") String eventName){
        
        return this.promoService.deactivateAllCodes(eventName);
    }
    
    @PostMapping(value = "edit")
    ResponseEntity edit(){
        
        return new ResponseEntity(HttpStatus.OK);
    }
       
    //Get all promo codes
    @GetMapping(value = "all")
    ResponseEntity all(){
        
        return this.promoService.getAllCode();
    }
    
    //Get all active promo codes
    @GetMapping(value = "all/active")
    ResponseEntity allActive(){
        
        return this.promoService.getAllActiveCodes();
    }
    
    //Check id promo code is valid 
    @PostMapping(value = "validate")
    ResponseEntity validate(@RequestBody @Valid ValidateRequest request){
        
        return this.promoService.validatePromoCode(request);
        
    }
    
}
